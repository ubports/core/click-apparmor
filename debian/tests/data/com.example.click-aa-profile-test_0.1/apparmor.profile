# Test package profile
#include <tunables/global>
# Specified profile variables
###VAR###
###PROFILEATTACH### {
  #include <abstractions/base>
  # Read-only for the install directory
  @{CLICK_DIR}/@{APP_PKGNAME}/                   r,
  @{CLICK_DIR}/@{APP_PKGNAME}/@{APP_VERSION}/    r,
  @{CLICK_DIR}/@{APP_PKGNAME}/@{APP_VERSION}/**  mrklix,
}
